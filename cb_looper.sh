#!/bin/bash
#variables
export var_nse='grab_beacon_config.nse'
export var_error='beacon_log.csv'
export var_sssout='single_scan.json'
export var_c2_store='cb_beacons'
export var_s2_store='sho_beacons'
export var_j2_store='ja3_beacons'
export var_conn='https://google.com'
export var_stmplog='s_tmp.log'
export var_ltmplog='l_tmp.log'
export var_good='0'
export var_badi='0'
export var_time=$(date +%d-%m-%y)
export var_session=$(openssl rand -hex 12)
export var_nse_authors='Wade Hickey and Zach Stanford'
export var_apikey=$(cat bruce_willis.timetravel |jq -r .apikey)
export var_slack=$(cat bruce_willis.timetravel |jq -r .slack)
#functions
trap ctrl_c INT
function ctrl_c() {
    echo "[failure]interrupt trapped"
    echo -n "$var_time," >> "$var_error"
	echo "init,$var_session,user interrupted" >> "$var_error"
    rm x.log 2>/dev/null
    tail -n 2 "$var_error"
	exit 1
}
clean_it_up () {
    rm $var_stmplog $var_stmplog $var_sssout x.log 2>/dev/null
}
logg_startup () {
    echo -n "$var_time," >> "$var_error"
	echo "init,$var_session,session started" >> "$var_error"
}
logg_shutdown () {
    echo -n "$var_time," >> "$var_error"
	echo "init,$var_session,session finished" >> "$var_error"
    rm x.log 2>/dev/null
}
output_check () {
mkdir $var_c2_store $var_s2_store $var_j2_store 2>/dev/null
}
meme_banner () {
    echo '''⣿⣿⠟⠛⠛⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⢋⣩⣉⢻⣿⣿
⣿⣿⠀⣿⣶⣕⣈⠹⠿⠿⠿⠿⠟⠛⣛⢋⣰⠣⣿⣿⠀⣿⣿
⣿⣿⡀⣿⣿⣿⣧⢻⣿⣶⣷⣿⣿⣿⣿⣿⣿⠿⠶⡝⠀⣿⣿
⣿⣿⣷⠘⣿⣿⣿⢏⣿⣿⣋⣀⣈⣻⣿⣿⣷⣤⣤⣿⡐⢿⣿
⣿⣿⣿⣆⢩⣝⣫⣾⣿⣿⣿⣿⡟⠿⠿⠦⠀⠸⠿⣻⣿⡄⢻
⣿⣿⣿⣿⡄⢻⣿⣿⣿⣿⣿⣿⣿⣿⣶⣶⣾⣿⣿⣿⣿⠇⣼
⣿⣿⣿⣿⣿⡄⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⣰⣿
[init]collector running...'''
}
file_check_nse () {
if [[ ! -f $var_nse ]]
    then
	echo "[failure] check event log"
	echo -n "$var_time," >> "$var_error"
	echo "nse,nse script not found" >> "$var_error"
    tail -n 2 "$var_error"
    exit 1
fi
}
shodan_check () {
if [ $(cat bruce_willis.timetravel |jq -r .apikey) ]
    then
    echo ""
    else
	echo "[failure] check event log"
	echo -n "$var_time," >> "$var_error"
	echo "shodan,no api key" >> "$var_error"
    tail -n 2 "$var_error"
    exit 1
fi
}
get_conn () {
n=0
	until [ "$n" -ge 15 ]
	do
    curl -s --insecure "$var_conn" >/dev/null && break
    n=$((n+1))
    sleep 5
    done
	if [ "$n" == 0 ]
        then
        sleep 0.1
        elif [ "$n" -gt 0 ]
        then
		echo "[failure] check event log"
		echo -n "$var_time," >> "$var_error"
		echo "inet,connection,internet failed" >> "$var_error"
		$var_clean
        tail -n 2 "$var_error"
		exit 1
	fi
}
show_details () {
    echo "[init]completed"
    echo "[init]success count $(echo $var_good)"
    echo "[init]failure count $(echo $var_badi)"
}
check_slacks () {
    echo "running slack configuration checks"
    sleep 2
if [ -z $(cat bruce_willis.timetravel |jq -r .slack) ]
    then
    echo "ERROR: check slack webhook config"
    exit 1
fi
if [ -z $(cat bruce_willis.timetravel |jq -r .slackbot) ]
    then
    echo "ERROR: check slack bot config"
    exit 1
fi
if [ -z $(cat bruce_willis.timetravel |jq -r .slackchannel) ]
    then
    echo "ERROR: check slack channel config"
    exit 1
fi
}
#
clear
#main routine
if [ $# -lt 2 ]
    then
    meme_banner
    echo "=================================================================="
    echo "[use]bash cb_looper.sh --single ip"
    echo "[use]bash cb_looper.sh --list listofips.txt"
    echo "=================================================================="
    echo "docker run --rm -ti -v ${PWD}:/cb_looper cb_looper --single ip"
    echo "docker run --rm -ti -v ${PWD}:/cb_looper cb_looper --list list.txt"
    echo -n "$var_time," >> "$var_error"
	echo "init,$var_session,session wrong args" >> "$var_error"
    exit 1
fi
if [ "$1" == --help ]
    then
    meme_banner
    echo "[use]bash cb_looper.sh --single ip"
    echo "[use]bash cb_looper.sh --list listofips.txt"
    echo -n "$var_time," >> "$var_error"
	echo "init,$var_session,session --help" >> "$var_error"
    exit 1
fi
echo -n "Are you using slack webhook? (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    check_slacks
    echo "WARNING: Slack webhooks will only post if C2 data is identified!"
    sleep 5
fi
#logging
clean_it_up
logg_startup
output_check
meme_banner
echo "Thankyou $var_nse_authors !!!"
if [ $(cat bruce_willis.timetravel |jq -r .slack) ]
    then
    echo "Looper Slack enabled"
    echo -n "$var_time," >> "$var_error"
    echo "slack,api,access enabled" >> "$var_error"
fi
shodan_check
file_check_nse
#actual stuff
if [ "$1" == --single ]
    then
    get_conn
    if [ $(cat bruce_willis.timetravel |jq -r .apikey) ]
        then
        echo "$2: shodan connections success"
        echo -n "$var_time," >> "$var_error"
        echo "shodan,api access enabled" >> "$var_error"
        rm shodan.json ja3er.json 2>/dev/null
        curl --retry 10 --connect-timeout 30 -s -X GET 'https://api.shodan.io/shodan/host/'$2'?key='$(cat bruce_willis.timetravel |jq -r .apikey) |jq > shodan.json
        if [ -s shodan.json ]
            then
            echo "$2: shodan enumeration success"
            echo -n "$var_time," >> "$var_error"
            echo "shodan,$2,shodan export success" >> "$var_error"
            curl --retry 10 --connect-timeout 30 -s -X GET 'https://ja3er.com/search/'$(cat shodan.json |jq -r '.data[].ssl.ja3s' |grep -v "null" |uniq) |jq > ja3er.json
            if [ -s ja3er.json ]
                then
                echo "$2: ja3hsh enumeration success"
                echo -n "$var_time," >> "$var_error"
                echo "ja3hsh,$2,ja3hsh export success" >> "$var_error"
                if [ `grep -o "Sorry" ja3er.json` ]
                    then
                    echo "$2: ja3hsh not found"
                    echo -n "$var_time," >> "$var_error"
                    echo "ja3hsh,$2,ja3hsh not found" >> "$var_error"
                    else
                    echo "$2: ja3hsh hash found"
                    echo -n "$var_time," >> "$var_error"
                    echo "ja3hsh,$2,ja3hsh hash found" >> "$var_error"
                fi
                mv ja3er.json $var_j2_store/$var_session-ja3-$2.json
                else
                echo "$2: ja3hsh enumeration failure"
                echo -n "$var_time," >> "$var_error"
                echo "ja3hsh,$2,ja3hsh export failure" >> "$var_error"
                mv ja3er.json $var_j2_store/$var_session-ja3-$2.json
            fi
        mv shodan.json $var_s2_store/$var_session-shodan-$2.json
        else
        echo "$2: shodan enumeration failure"
        echo -n "$var_time," >> "$var_error"
        echo "shodan,$2,export failure" >> "$var_error"
        shodan.json 2>/dev/null
    fi
    rm shodan.json 2>/dev/null
    else
    echo "$2: shodan connections failure"
    echo -n "$var_time," >> "$var_error"
    echo "shodan,$2,connection failure" >> "$var_error"
    fi
    sudo nmap --host-timeout 120 $2 -script=grab_beacon_config.nse > $var_stmplog
    echo -n "$var_time," >> "$var_error"
    echo "nmap,$2,nmap completed" >> "$var_error"
    if grep -q  "{" $var_stmplog
        then
        cat $var_stmplog |grep "{" | sed 's/|\_grab_beacon_config\:\ //g' |jq > $var_sssout
        if [ -f $var_sssout ]
            then
            if [ $(wc -l $var_sssout |cut -f1 -d " ") -gt 40 ]
                then
                echo "$2: beacon corrleation major"
                echo -n "$var_time," >> "$var_error"
                echo "c2,$2,major config" >> "$var_error"
                else
                echo "$2: beacon corrleation minor"
                echo -n "$var_time," >> "$var_error"
                echo "c2,$2,minor config" >> "$var_error"
            fi
            rm $var_stmplog 2>/dev/null
            mv -f $var_sssout $var_c2_store/$var_session-$2.json
            echo "$var_c2_store/$var_session-$2.json" >> x.log
            echo -n "$var_time," >> "$var_error"
            echo "c2,$2,beacon exists" >> "$var_error"
            exit 1
            else
            echo "$2: beacon corrleation failed"
            rm $var_stmplog 2>/dev/null
            rm $var_stmplog 2>/dev/null
            echo -n "$var_time," >> "$var_error"
            echo "c2,$2,no content" >> "$var_error"
        fi
        else
        echo "$2: beacon corrleation failed"
        rm $var_stmplog 2>/dev/null
        rm $var_sssout 2>/dev/null
        echo -n "$var_time," >> "$var_error"
        echo "c2,$2,no beacon" >> "$var_error"
    fi
fi
if [ "$1" == --list ]
    then
    get_conn
    while read line
        do
        if [ $(cat bruce_willis.timetravel |jq -r .apikey) ]
            then
            echo "$line: shodan connections success"
            rm shodan.json ja3er.json 2>/dev/null
            curl --retry 10 --connect-timeout 30 -s -X GET 'https://api.shodan.io/shodan/host/'$line'?key='$(cat bruce_willis.timetravel |jq -r .apikey) |jq > shodan.json
            if [ -s shodan.json ]
                then
                echo "$line: shodan enumeration success"
                echo -n "$var_time," >> "$var_error"
                echo "shodan,$line,export success" >> "$var_error"
                curl --retry 10 --connect-timeout 30 -s -X GET 'https://ja3er.com/search/'$(cat shodan.json |jq -r '.data[].ssl.ja3s' |grep -v "null" |uniq) |jq > ja3er.json
                if [ -s ja3er.json ]
                    then
                    echo "$line: ja3hsh enumeration success"
                    echo -n "$var_time," >> "$var_error"
                    echo "ja3hsh,$line,export success" >> "$var_error"
                    if [ `grep -o "Sorry" ja3er.json` ]
                        then
                        echo "$line: ja3hsh hash not found"
                        echo -n "$var_time," >> "$var_error"
                        echo "ja3hsh,$line,hash not found" >> "$var_error"
                        else
                        echo "$line: ja3hsh hash found"
                        echo -n "$var_time," >> "$var_error"
                        echo "ja3hsh,$line,hash found" >> "$var_error"
                    fi
                    mv ja3er.json $var_j2_store/$var_session-ja3-$2.json
                    else
                    echo "$line: ja3hsh enumeration failure"
                    echo -n "$var_time," >> "$var_error"
                    echo "ja3hsh,$line,ja3hsh export failure" >> "$var_error"
                    mv ja3er.json $var_j2_store/$var_session-ja3-$2.json
                fi
            mv shodan.json $var_s2_store/$var_session-shodan-$2.json
            else
            echo "$line: shodan enumeration failure"
            echo -n "$var_time," >> "$var_error"
            echo "shodan,$line,export failure" >> "$var_error"
            shodan.json 2>/dev/null
        fi
        rm shodan.json 2>/dev/null
        else
        echo "$line: shodan connections failure"
        echo -n "$var_time," >> "$var_error"
        echo "shodan,$line,connection failure" >> "$var_error"
        fi
        sudo nmap --host-timeout 120 $line -script=grab_beacon_config.nse > $var_ltmplog
        echo -n "$var_time," >> "$var_error"
        echo "nmap,$line,nmap completed" >> "$var_error"
        if [ -s $var_ltmplog ]
            then
            cat  $var_ltmplog |grep "{" | sed 's/|\_grab_beacon_config\:\ //g' |jq > $line.json 2> /dev/null
                if [ -s $line.json ]
                    then
                    if [ $(wc -l $line.json |cut -f1 -d " ") -gt 40 ]
                        then
                        echo "$line: beacon corrleation major"
                        echo -n "$var_time," >> "$var_error"
                        echo "c2,$line,major config" >> "$var_error"
                        else
                        echo "$line: beacon corrleation minor"
                        echo -n "$var_time," >> "$var_error"
                        echo "c2,$line,minor config" >> "$var_error"
                    fi
                    mv -f $line.json $var_c2_store/$var_session-$line.json
                    echo "$var_c2_store/$var_session-$line.json" >> x.log
                    rm $var_ltmplog 2>/dev/null
                    var_good=$((var_good + 1))
                    else
                    echo "$line: beacon corrleation failed"
                    rm $line.json 2>/dev/null
                    rm $var_ltmplog 2>/dev/null
                    echo -n "$var_time," >> "$var_error"
                    echo "c2,$line,no config" >> "$var_error"
                    var_badi=$((var_badi + 1))
                fi
            else
            echo "$line: beacon corrleation failed"
            rm $var_ltmplog 2>/dev/null
            echo -n "$var_time," >> "$var_error"
            echo "c2,$line,no beacon" >> "$var_error"
            var_badi=$((var_badi + 1))
        fi
        done < "$2"
if [ $(cat bruce_willis.timetravel |jq -r .slack) ]
    then
    export var_bot=$(cat bruce_willis.timetravel |jq -r .slackbot)
    export var_chn=$(cat bruce_willis.timetravel |jq -r .slackchannel)
    export var_ohshit=$(wc -l x.log |cut -f1 -d " ")
    curl -s -m 10 --retry 10 --connect-timeout 10 -X POST --data-urlencode "payload={\"channel\": \"$var_chn\", \"username\": \"$var_bot\", \"text\": \"ID: CB Looper\nFunction: Cobalt Strike C2 detection\nTime: $var_time\nActive C2: $var_ohshit\"}" "$var_slack" > /dev/null
    echo -n "$var_time," >> "$var_error"
    echo "slack,message,success" >> "$var_error"
fi
show_details
logg_shutdown
fi