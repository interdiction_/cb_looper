#docker build -t cblooper . --no-cache
#docker run --rm -ti -v ${PWD}:/cb_looper cb_looper
FROM kalilinux/kali-rolling 
LABEL cblooper "inteloperator"
RUN apt update
RUN apt install jq curl nmap grep -y
WORKDIR "/cb_looper"
ENTRYPOINT ["bash", "cb_looper.sh"]