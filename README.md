<div align="center">

![codebase](https://img.shields.io/badge/codebase-bash-black) ![codebase](https://img.shields.io/badge/codebase-docker-blue)
![codebase](https://img.shields.io/badge/API-shodan-red) ![codebase](https://img.shields.io/badge/API-ja3-blue)

<img src="images/42agWcc69N082da400-3b5f-4ab2-bc84-cdf95b678c6b-1630745689.png" width="150">

**Leverages nmap to extract and parse cobalt strike C2 configs**

</div>

```
git clone https://gitlab.com/inteloperator/cb_looper.git && cd cb_looper
bash cb_looper.sh --help
```

**What does what?**

```
bash cb_looper_check_deps.sh #Checks and installs required deps!
bash cb_looper.sh --single ip #Single cobalt strike C2
bash cb_looper.sh --list listofips.txt #List of cobalt strike C2's
```

**Dockerfile**

```
docker build -t cb_looper . --no-cache
docker run --rm -ti -v ${PWD}:/cb_looper cb_looper
```

```
FROM kalilinux/kali-rolling 
LABEL cblooper "inteloperator"
RUN apt update
RUN apt install jq curl nmap grep -y
WORKDIR "/cb_looper"
ENTRYPOINT ["bash", "cb_looper.sh"]
```

<div align="center">
<img src="images/BxYnZy5EqT6de813b2-5408-4b80-8d86-aa5da5f5243e-1632553081.png" width="400">
</div>

**Updates**

- [ ] :computer: Add a react web app to visualize exported data
- [x] :computer: Logging has been updated to work with ELK
- [x] :computer: Has a docker version `docker build -t cblooper . --no-cache`
- [x] :computer: Uses shodan for correlation against ja3 hashes
- [x] :computer: Uses ja3er API for ja3 hash correlation/identification
- [x] :computer: Has slack webhook notifications
- [x] :computer: Leverages json config file `bruce_willis.timetravel`

```
{
    "apikey": "SHODANAPI",
    "slack": "https://hooks.slack.com/services/BLAH/BLAH/BLAH",
    "slackbot": "YOURBOT",
    "slackchannel": "#YOURCHANNEL"
}
```

<div align="center">
<img src="images/PsUgwUvp3r12aad424-2317-4cc3-90db-1bf77547c3e4-1632553130.png" width="400">
</div>

**Points to note**

- [x] Make sure your input file has LF not CRLF line endings _dos2unix..._
- [x] Outputs configs in (current working dir)cb_beacons, ja3_beacons and sho_beacons
- [x] Error correction on each stage of the parse
- [x] Checks for major and minor cobalt C2 beacon types
- [x] Provides logging and summary on each pass; and
- [x] Check images below for examples.

_Thanks to [whickey-r7](https://github.com/whickey-r7/grab_beacon_config/blob/main/grab_beacon_config.nseurl) for the nse config!_
